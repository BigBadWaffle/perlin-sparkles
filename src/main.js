const config = {
	radius: 6,
	padding: 4,
	brightness: 2,
	colors: [{
		r: 74,
		g: 92,
		b: 182
	}, {
		r: 238,
		g: 8,
		b: 197
	}, {
		r: 200,
		g: 201,
		b: 202
	}]
};

let noise = new SimplexNoise();

let builder = {
	ctx: null,

	x: 0,
	y: 0,
	t: 0,

	init: function () {
		let canvas = document.getElementsByTagName('canvas')[0];
		let ctx = this.ctx = canvas.getContext('2d');

		let body = document.body;
		this.width = canvas.width = body.clientWidth;
		this.height = canvas.height = body.clientHeight;

		requestAnimationFrame(this.render.bind(this));
	},

	perlin: function (x, y, div, mult) {
		return Math.abs(noise.noise3D(x / div, y / div, this.t) * mult);
	},

	render: function () {
		let ctx = this.ctx;
		
		ctx.clearRect(0, 0, this.width, this.height);

		let radius = config.radius;
		let diameter = radius * 2;
		let pi = Math.PI;

		let padding = config.padding;

		let w = this.width / (padding + diameter);
		let h = this.height / (padding + diameter);

		let min = 100;
		let max = 0;

		let iColor = 100;
		let jColor = 100;
		let dColor = 150;
		let mColor = 1;

		for (let i = 0; i < w; i++) {
			let x = radius + padding + (i * (diameter + padding));
			for (let j = 0; j < h; j++) {
				let y = radius + padding + (j * (diameter + padding));

				ctx.globalAlpha = this.perlin(i, j, 5, 2);
					
				let rollColor = (
					(this.perlin(i + iColor + this.x, j + jColor + this.y, dColor, mColor) * 20) + 
					(this.perlin(i - iColor + this.x, j - jColor + this.y, dColor * 10, mColor) * 7) + 
					(this.perlin(i + this.x, j + jColor + this.y, dColor * 30, mColor) * 1)
				) / 28;
				let colorA = null;
				let colorB = null;
				let f = 0;

				if (rollColor < 0.2) {
					colorA = config.colors[0];
					colorB = config.colors[1];
					f = (rollColor / 0.3);
				} else {
					colorA = config.colors[1];
					colorB = config.colors[2];
					f = (rollColor - 0.3) / 0.1;
				}

				let color = {
					r: colorA.r + ((colorA.r - colorB.r) * f),
					g: colorA.g + ((colorA.g - colorB.g) * f),
					b: colorA.b + ((colorA.b - colorB.b) * f)
				};

				ctx.fillStyle = `rgb(${color.r}, ${color.g}, ${color.b})`; 

				ctx.beginPath();
				ctx.arc(x, y, radius, 0, 2 * pi);
				ctx.fill();
				ctx.closePath();
			}
		}

		console.log(min, max);

		this.x++;
		this.y++;

		this.t += 0.005;

		requestAnimationFrame(this.render.bind(this));
	}
};

builder.init();
